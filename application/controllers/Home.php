<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends CI_Controller {
	
	function __construct(){
		parent::__construct();
		header('Access-Control-Allow-Origin: *');
        header("Access-Control-Allow-Methods: GET, POST, OPTIONS, PUT, DELETE");
		$this->load->helper('url');		
	}

	public function index(){		
		$this->load->view('index');
	}

	public function validation(){		
		//$this->load->library('phpmailer_lib');
		$this->load->library('form_validation');		
		$this->form_validation->set_rules('nome', 'Name', 'required', 
		array('required' => 'O campo deve ser preenchido!'));
		$this->form_validation->set_rules('email', 'Email', 'required|valid_email', 
		array('required' => 'O campo deve ser preenchido!',
				'valid_email' => 'O campo deve ser válido!'));
		$this->form_validation->set_rules('assunto', 'Assunto', 'required', 
		array('required' => 'O campo deve ser preenchido!'));
		$this->form_validation->set_rules('mensagem', 'Mensagem', 'required', 
		array('required' => 'O campo deve ser preenchido!'));
		$this->form_validation->set_error_delimiters('<div class="validate">', 
		'</div>');
		/*
		$name = $this->input->post('nome'); //pega os dados que foi digitado no ID name.
		$email = $this->input->post('email'); //pega os dados que foi digitado no ID email.
		$tel = $this->input->post('telefone'); //pega os dados que foi digitado no ID email.
		$subject = 'Dúvida/serviço'; //pega os dados que foi digitado no ID sebject (assunto).
		$message = $this->input->post('mensagem'); //pega os dados que foi digitado no ID message.
		$myEmail = "contato@desentupipel.com.br"; //é necessário informar um e-mail do próprio domínio
		$headers = "From: $myEmail\r\n";
		$headers .= "Reply-To: $email\r\n";
		/*abaixo contém os dados que serão enviados para o email cadastrado para receber o formulário*/
		/*$corpo = "Formulário enviado pela página (site): www.desentupipel.com.br\n";
		$corpo .= "Nome: " . $name . "\n";
		$corpo .= "Email: " . $email . "\n";
		$corpo .= "Telefone/celular: " . $tel . "\n";
		$corpo .= "Mensagem: " . $message . "\n";
		$email_to = 'desentupipel@gmail.com'; //não esqueça de substituir este email pelo seu.*/
		
		if($this->form_validation->run()){	
			//$status = mail($email_to, $subject, $corpo, $headers);
			$array = array(				
				'success' => '<div class="alert alert-success">
				<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
				<strong>Obrigado por fazer contato. Retornaremos!</strong></div>'
			);				
		} else {
			$array = array(
				'error' => true,
				'name_error' => form_error('nome'),
				'email_error' => form_error('email'),
				'assunto_error' => form_error('assunto'),
				'message_error' => form_error('mensagem')				
			);		
		}
		echo json_encode($array);
	}	

	public function khelpdesk(){
		$this->load->helper('download');
		force_download('./assets/suporte/KHelpDesk.exe', NULL);
	}
}
